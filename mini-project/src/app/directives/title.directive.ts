import { Directive, ElementRef } from '@angular/core';

@Directive({
  selector: '[appTitle]'
})
export class TitleDirective {

  constructor(private el: ElementRef) {
    el.nativeElement.style.color= 'blue'

   }
 
}
