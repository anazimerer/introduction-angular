import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar'


@Injectable({
  providedIn: 'root'
})
export class ProductsService {

  //injetando o snak bar
constructor( private snackBar: MatSnackBar) { }

showMessage(msg: string) {
  this.snackBar.open(msg, 'X',{
    duration: 3000,
    horizontalPosition: "right",
    verticalPosition: "top",
    
  })
}

}
