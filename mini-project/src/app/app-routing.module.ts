import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GraphicComponent } from './views/graphic/graphic.component';
import { TableComponent } from './components/template/table/table.component';
import { FormsComponent } from './components/template/forms/forms.component';
import { InputsComponent } from './components/template/inputs/inputs.component'
import { ProductsComponent } from './components/template/products/products.component'

const routes: Routes = [
  { path: "graphic", component: GraphicComponent },
  { path: "table", component: TableComponent },
  { path: "forms", component: FormsComponent },
  { path: "inputs", component: InputsComponent },
  { path: "products", component: ProductsComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
